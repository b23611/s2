package com.zuitt.example;
// This import is for getting user inputs
import java.util.Scanner;

public class UserInput {
    public static void main(String[] args){
        Scanner myObj = new Scanner(System.in);
        System.out.println("Enter username: ");
        // .nextLine Ends when input has been entered, it only captures string
        String userName = myObj.nextLine();
//        System.out.println("Username is: " + userName);

//        System.out.println(userName userName);
    }
}
