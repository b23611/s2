package com.zuitt.example;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;

public class Array {
        //[SECTION] Java Collection
        // are a single units of objects.
        // useful for manipulating relevant pieces of data tha can be used in different situations, more commonly with loops.

        public static void  main(String[] args){
                // [SECTION] Array
                //In java, arrays are containers of values of the same data type given  a predefined amount of values.
                // java arrays are more rigid, once the size and data type are defined, they can no longer change.

                // Syntax: Array Declaration
                // dataType[] identifier = new dataType[numOfElements];
                // "[]" indicates that data type should be able to hold multiple values.
                // "new" keyword is used for non-primitive data types to tell Java to create the said variable.
                // the values of the array is initializes to 0 or null.

                int[] intArray = new int[5];

                intArray[0] = 200;
                intArray[1] = 3;
                intArray[2] = 25;
                intArray[3] = 50;
                intArray[4] = 98;

                // This will return the memory address of the array.
//            System.out.println(intArray);
                // To print the intArray, we need to import the "Arrays" Class and use the .toString() method to convert the array to string.
//            System.out.println(Arrays.toString(intArray));

                //Syntax: Array Declaration with initialization
                // dataType[] identifier = {elementA, elementB, elementC, ... elementNth};
                // the compiler automatically specifies the size by counting the number of elements in the array
                String[] names = {"John", "Jane", "Joe"};
                // names[4] = "Joey"; // out of bounce length error.

//            System.out.println(Arrays.toString(names));

                // Sample java array methods:

                //Sort
                Arrays.sort(intArray);
//            System.out.println("Order of items after sort: "+ Arrays.toString(intArray));

                // Multidimensional arrays
                // A two-dimensional array can be described as two lengths of nested array within each other, like a matrix.
                //first length is row, second length is column.
                // Syntax: dataType[][] identifier = new dataType[rowLength][colLength];
                String[][] classroom = new String[3][3];

                //First row
                classroom[0][0] = "Athos";
                classroom[0][1] = "Porthos";
                classroom[0][2] = "Aramis";
                //Second row
                classroom[1][0] = "Brandon";
                classroom[1][1] = "JunJun";
                classroom[1][2] = "Jobert";
                //Third row
                classroom[2][0] = "Mickey";
                classroom[2][1] = "Donald";
                classroom[2][2] = "Goofy";

                //we use the deepToString() method for printing multidimensional array.
//            System.out.println(Arrays.deepToString(classroom));

                //In Java, the size of the array cannot be modified. If there is a need to add or remove elements, new arrays must be created.

                //ArrayLists
                // are resizable arrays, wherein elements can be added or removed whenever it is needed.
                // Syntax:
                // ArrayList<dataType> identifier = new ArrayList<dataType>();

                //Declare an ArrayList
//                ArrayList<String> students = new ArrayList<String>();

                //Declare an arrayList with values
                ArrayList<String> students = new ArrayList<String>(Arrays.asList(names));

                // Add element
                // arrayListName.add(element);
                students.add("John");
                students.add("Paul");
                System.out.println(students);

                // Access element
                // arrayListName.get(index);
                System.out.println(students.get(1));


                // Adding an element on a specific index
                //arrayListName.add(index, value);
                students.add(1,"Mike");
                System.out.println(students);

                // Updating an element
                //arrayListName.set(index, element);
                students.set(1,"George");
                System.out.println(students);

                //Remove an element
                //arrayListName.remove(index);
                students.remove(1);
                System.out.println(students);

                //Remove all the elements
                //arrayListName.clear()
                students.clear();
                System.out.println(students);

                //Getting arrayList size
                //arrayList.size()
                System.out.println(students.size());

                //[SECTION] Hashmaps
                // most objects in Java are defined and are instantiations of Classes that contain a proper set of properties and methods.
                // There are might be use cases where is this not appropriate, or you may simply want to store a collection of data in key-value pairs
                // in Java "keys" also referred as "fields"
                // wherein the values are accessed by the fields
                // Syntax:
                // HashMap<dataTypeField, dataTypeValue> identifier = new HashMap<>();

                //Declaring Hashmaps
                HashMap<String, String> jobPosition =  new HashMap<String, String>();

                // Add element
                // hashMapName.put(<field>, <value>);
                jobPosition.put("Student", "Brandon");
                jobPosition.put("Dreamer", "Alice");
                //jobPosition.put("Student", "John"); Same field will override whenever there are duplicate keys.
                System.out.println(jobPosition);

                // Access element
                // hashMapName.get("field")
                System.out.println(jobPosition.get("Dreamer"));

                //Updating the values
                //hashMapName.replace(ketToChanged, newValue);
                jobPosition.replace("Student", "Brandon Smith");
                System.out.println(jobPosition);

                // Remove an element
                // hashMapName.remove(key/field);
                jobPosition.remove("Dreamer");
                System.out.println(jobPosition);

                // Clear all the content
                // hashMapNames.clear();
                jobPosition.clear();
                System.out.println(jobPosition);


    }
}
