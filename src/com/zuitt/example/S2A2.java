package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class S2A2 {
    public static void main(String[] args){
        int[] intArray = new int[5];

        intArray[0] = 2;
        intArray[1] = 3;
        intArray[2] = 5;
        intArray[3] = 7;
        intArray[4] = 11;

        System.out.println("The Fourth Prime Number is: " + intArray[3]);

        String names = null;
        ArrayList<String> students = new ArrayList<String>(Arrays.asList(names));
        students.add("John");
        students.add("Jane");
        students.add("Chloe");
        students.add("Zoe");

        System.out.println("My friends are: " + students);

        HashMap<String, Integer> inventory =  new HashMap<String, Integer>();

        inventory.put("toothpaste", 15);
        inventory.put("tootbrush", 20);
        inventory.put("soap", 12);

        System.out.println("Our current inventory consists of: " + inventory);


    }
}
