package com.zuitt.example;

import  java.util.Scanner;

public class ControlStructure {
    public static void main(String[] args){
        //[SECTION] Java Operators
            //Arithmetic -> +, -, *, /, %
            //Comparison -> >, <, >=, <=, ==, !=
            //Logical -> &&, ||, !
            //Assignment -> =

        //[SECTION] Selection Control Structure in Java
            //if else

            int num = 36;
            if(num % 5 == 0){
                System.out.println(num + " is divisible by 5");
            } else {
                System.out.println(num + " is not divisible by 5");
            }

         //[SECTION] Short Circuiting
            // a technique applicable only to the AND & OR operators wherein if-statements or other control structures can exit early by ensuring either safety of operation or efficiency.
            // right hand operand is not evaluated
            // OR operator
            // (true || ...) = true
            //AND operator
            // (false || ...) = false
            // This is helpful to prevent run time errors.
            int x = 15;
            int y = 0;

            if(y != 0 && x/y == 0){
                System.out.println("Result is: " + x/y);
            }
        //[SECTION] Ternary  Operator
            int num1 = 24;
            Boolean result = (num1 > 0) ? true : false;

        // [Section] Switch Cases
            Scanner numberScanner = new Scanner(System.in);
            System.out.println("Enter a number: ");
            int directionValue = numberScanner.nextInt();

            switch (directionValue){
                case 1:
                    System.out.println("North");
                    break;
                case 2:
                    System.out.println("South");
                    break;
                case 3:
                    System.out.println("West");
                    break;
                case 4:
                    System.out.println("East");
                    break;
                default:
                    System.out.println("Invalid");
            }
    }
}
